package br.com.angrum.backoffice.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.angrum.backoffice.facade.IBackofficeManagementFacade;
import br.com.angrum.backoffice.util.session.Access;
import br.com.angrum.backoffice.util.session.Credentials;
import br.com.angrum.backoffice.util.session.JTWUtil;

@Path("/auth")
@RequestScoped
public class AuthenticationService {

	@Inject
	private IBackofficeManagementFacade backofficeManagementFacade;

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMember(Access access) {
		//Credentials credentials = backofficeManagementFacade.login(access);
		Credentials credentials = new Credentials();
		credentials.setLogin("ADMIN");
		if ("ADMIN".equals(access.getUsername()) && "ADMIN".equals(access.getPassword())) {
			String token = JTWUtil.create(credentials.getLogin());
			credentials.setToken(token);
			return Response.ok().entity(credentials).build();
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

}
