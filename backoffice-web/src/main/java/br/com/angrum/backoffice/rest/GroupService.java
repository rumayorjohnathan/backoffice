package br.com.angrum.backoffice.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.angrum.backoffice.entity.Group;
import br.com.angrum.backoffice.facade.IBackofficeManagementFacade;
import br.com.angrum.backoffice.filter.GroupFilter;

@Path("/group")
@RequestScoped
public class GroupService {

	@Inject
	private IBackofficeManagementFacade backofficeManagementFacade;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createGroup(Group group) {
		try {
			backofficeManagementFacade.save(group);
			return Response.status(Response.Status.CREATED).build();
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateGroup(Group group) {
		try {
			backofficeManagementFacade.save(group);
			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (IllegalArgumentException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteGroup(@PathParam("id") Long id) {
		try {
			backofficeManagementFacade.delete(id);
			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findGroup(@PathParam("id") Long id) {
		try {
			Group group = backofficeManagementFacade.findById(id);
			return Response.status(Response.Status.OK).entity(group).build();
		} catch (Exception e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchGroup(GroupFilter groupFilter) {
		try {
			List<Group> groups = backofficeManagementFacade.search(groupFilter);
			return Response.status(Response.Status.OK).entity(groups).build();
		} catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

}
