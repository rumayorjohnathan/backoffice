package br.com.angrum.backoffice.util.persistence;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Criteria;

@Local
public interface IPersistenceUtils<T extends IPojo<?>> extends Serializable {

	EntityManager getEntityManager();

	CriteriaBuilder getCriteriaBuilder();

	CriteriaQuery<T> createCriteriaQuery(Class<T> clazz);

	void logQuery(CriteriaQuery<T> criteriaQuery);

	List<T> getResultList(Criteria criteria);

	Set<T> getResultSet(Criteria criteria);

	List<T> getResultList(CriteriaQuery<T> criteriaQuery);

	Set<T> getResultSet(CriteriaQuery<T> criteriaQuery);

	T getSingleResult(Criteria criteria);

	T getSingleResult(CriteriaQuery<T> criteriaQuery);

	Criteria createCriteria(Class<T> clazz);

	Criteria createCriteria(Class<T> clazz, String alias);

	void save(T pojo);

	void saveAndFlush(T pojo);

	void saveAndDetach(T pojo);

	void saveList(List<T> listPojo);

	void removeList(List<T> listPojo);

	void remove(T pojo);
	
	void removeAndFlush(T pojo);

	T getPorId(Class<T> clazz, Object id);

	void flushAndClear();

	@SuppressWarnings("rawtypes")
	List<T> listarTodos(Class clazz);
}
