package br.com.angrum.backoffice.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.slf4j.Logger;

import br.com.angrum.backoffice.entity.User;
import br.com.angrum.backoffice.filter.UserFilter;
import br.com.angrum.backoffice.util.annotation.BackofficeLogger;
import br.com.angrum.backoffice.util.annotation.BackofficePersistence;
import br.com.angrum.backoffice.util.persistence.IPersistenceUtils;
import br.com.angrum.backoffice.util.session.Credentials;

@Named
@Stateless
public class UserRepository implements IUserRepository {

	private static final long serialVersionUID = -2201357585940759798L;
	
	@Inject
	@BackofficeLogger
	private Logger LOG;
	
	@Inject
	@BackofficePersistence
	private IPersistenceUtils<User> persistence;

	@Override
	public List<User> listAll() {
		LOG.debug("Listed all Users from database");
		return persistence.listarTodos(User.class);
	}

	@Override
	public List<User> search(UserFilter userFilter) {
		Criteria criteria = persistence.createCriteria(User.class, "user");
		if (userFilter.getId() != null) {
			criteria.add(Restrictions.idEq(userFilter.getId()));
		}
		if (StringUtils.isNotBlank(userFilter.getName())) {
			criteria.add(Restrictions.ilike("user.name", userFilter.getName(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(userFilter.getLogin())) {
			criteria.add(Restrictions.ilike("user.login", userFilter.getLogin(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(userFilter.getPhone())) {
			criteria.createAlias("user.phone", "phone", JoinType.LEFT_OUTER_JOIN);
			criteria.add(Restrictions.ilike("phone.number", userFilter.getPhone(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(userFilter.getCity())) {
			criteria.createAlias("user.addresses", "address", JoinType.LEFT_OUTER_JOIN);
			criteria.add(Restrictions.ilike("address.city", userFilter.getCity(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(userFilter.getEmail())) {
			criteria.createAlias("user.emails", "email", JoinType.LEFT_OUTER_JOIN);
			criteria.add(Restrictions.ilike("email.address", userFilter.getEmail(), MatchMode.ANYWHERE));
		}
		if (userFilter.getGroup() != null) {
			criteria.createAlias("user.group", "group", JoinType.LEFT_OUTER_JOIN);
			criteria.add(Restrictions.eq("group.id", userFilter.getGroup()));
		}
		
		return persistence.getResultList(criteria);
	}

	@Override
	public User findById(Long id) {
		LOG.debug("Searched User from database: id=", id);
		return persistence.getPorId(User.class, id);
	}

	@Override
	public void save(User user) {
		LOG.debug("Inserted User into database: user=", user.toString());
		persistence.save(user);
	}

	@Override
	public void delete(Long id) {
		LOG.debug("Deleted User from database: id=", id);
		persistence.getEntityManager().remove(id);
	}

	@Override
	public Credentials login(String login, String password) {
		Criteria criteria = persistence.createCriteria(User.class, "user");
		criteria.add(Restrictions.eq("user.login", login));
		criteria.add(Restrictions.eq("user.password", password));
		return null;
	}

}
