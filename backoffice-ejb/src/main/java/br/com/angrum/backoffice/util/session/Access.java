package br.com.angrum.backoffice.util.session;

import java.io.Serializable;

public class Access implements Serializable {

	private static final long serialVersionUID = -4988845150499320329L;

	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
