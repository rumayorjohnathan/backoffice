package br.com.angrum.backoffice.business;

import java.io.Serializable;

import javax.ejb.Local;

import br.com.angrum.backoffice.util.session.Access;
import br.com.angrum.backoffice.util.session.Credentials;

@Local
public interface IAuthenticationBusiness extends Serializable {
	
	Credentials login(Access access);

}
