package br.com.angrum.backoffice.filter;

import java.io.Serializable;

public class GroupFilter implements Serializable {

	private static final long serialVersionUID = -6035311622223009025L;

	private Long id;
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
