package br.com.angrum.backoffice.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;

import br.com.angrum.backoffice.entity.Group;
import br.com.angrum.backoffice.filter.GroupFilter;
import br.com.angrum.backoffice.util.annotation.BackofficeLogger;
import br.com.angrum.backoffice.util.annotation.BackofficePersistence;
import br.com.angrum.backoffice.util.persistence.IPersistenceUtils;

@Named
@Stateless
public class GroupRepository implements IGroupRepository {

	private static final long serialVersionUID = -2629632495486167522L;
	
	@Inject
	@BackofficeLogger
	private Logger LOG;
	
	@Inject
	@BackofficePersistence
	private IPersistenceUtils<Group> persistence;
	

	@Override
	public List<Group> listAll() {
		return persistence.listarTodos(Group.class);
	}

	@Override
	public List<Group> search(GroupFilter groupFilter) {
		Criteria criteria = persistence.createCriteria(Group.class, "group");
		if (groupFilter.getId() != null) {
			criteria.add(Restrictions.idEq(groupFilter.getId()));
		}
		if (StringUtils.isNotBlank(groupFilter.getName())) {
			criteria.add(Restrictions.ilike("group.name", groupFilter.getName(), MatchMode.ANYWHERE));
		}
		return persistence.getResultList(criteria);
	}

	@Override
	public Group findById(Long id) {
		return persistence.getPorId(Group.class, id);
	}

	@Override
	public void save(Group group) {
		persistence.save(group);
	}

	@Override
	public void delete(Long id) {
		persistence.getEntityManager().remove(id);

	}

}
