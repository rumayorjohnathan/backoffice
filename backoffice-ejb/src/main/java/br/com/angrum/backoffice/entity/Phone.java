package br.com.angrum.backoffice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.angrum.backoffice.enums.PhoneType;
import br.com.angrum.backoffice.util.persistence.IPojo;

@Entity
@Table(schema = IPojo.BACKOFFICE_SCHEMA, name = "phone")
@SequenceGenerator(schema = IPojo.BACKOFFICE_SCHEMA, name = "seq_phone_id", sequenceName = "seq_phone", allocationSize = 1, initialValue = 1)
public class Phone implements IPojo<Long> {

	private static final long serialVersionUID = 3648067916684054308L;

	private Long id;
	private String ddd;
	private String number;
	private PhoneType type;
	private User user;

	@Id
	@GeneratedValue(generator = "seq_phone_id", strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "ddd", length = 2)
	@NotNull(message = "DDD não pode estar vazio!")
	@NotEmpty(message = "DDD não pode estar vazio!")
	@Size(min = 1, max = 2, message = "DDD deve conter dois dígitos!")
	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	@Column(name = "number", length = 9)
	@NotNull(message = "Telefone não pode estar vazio!")
	@NotEmpty(message = "Telefone não pode estar vazio!")
	@Size(min = 8, max = 9, message = "Telefone deve conter oito ou nove dígitos!")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Tipo de Telefone não pode estar vazio!")
	public PhoneType getType() {
		return type;
	}

	public void setType(PhoneType type) {
		this.type = type;
	}

	@ManyToOne()
	@JoinColumn(name = "id_user", referencedColumnName = "id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ddd == null) ? 0 : ddd.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Phone other = (Phone) obj;
		if (ddd == null) {
			if (other.ddd != null)
				return false;
		} else if (!ddd.equals(other.ddd))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (type != other.type)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	

}
