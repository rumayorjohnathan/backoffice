package br.com.angrum.backoffice.repository;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import br.com.angrum.backoffice.entity.User;
import br.com.angrum.backoffice.filter.UserFilter;
import br.com.angrum.backoffice.util.session.Credentials;

@Local
public interface IUserRepository extends Serializable {
	
	List<User> listAll();
	
	List<User> search(UserFilter userFilter);
	
	User findById(Long id);
	
	void save(User user);
	
	void delete(Long id);
	
	Credentials login(String login, String password);


}
