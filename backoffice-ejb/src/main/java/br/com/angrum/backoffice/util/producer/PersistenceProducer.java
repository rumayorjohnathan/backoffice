package br.com.angrum.backoffice.util.producer;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.angrum.backoffice.util.annotation.BackofficePersistence;

public class PersistenceProducer {
	
	@Produces
	@PersistenceContext(unitName = "primary")
	@BackofficePersistence
	private EntityManager em;

}
