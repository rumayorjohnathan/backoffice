package br.com.angrum.backoffice.enums;

public enum PhoneType {

	CELLPHONE(1, "Celular"), HOME(2, "Casa"), OFFICE(3, "Trabalho"), FAX(4, "Fax");

	private PhoneType(Integer id, String description) {
		this.id = id;
		this.description = description;
	}

	private Integer id;
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
