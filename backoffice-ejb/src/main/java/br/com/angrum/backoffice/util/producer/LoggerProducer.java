package br.com.angrum.backoffice.util.producer;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.angrum.backoffice.util.annotation.BackofficeLogger;

public class LoggerProducer implements Serializable {

	private static final long serialVersionUID = -4475424521017304854L;
	
	@Produces
	@BackofficeLogger
	public Logger produceLog(InjectionPoint injectionPoint) {
		return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
	}

}
