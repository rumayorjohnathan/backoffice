package br.com.angrum.backoffice.util.session;

import java.io.Serializable;
import java.util.Date;

import br.com.angrum.backoffice.entity.Group;

public class Credentials implements Serializable {

	private static final long serialVersionUID = -5507968299653208713L;

	private Long id;
	private String name;
	private String login;
	private Group group;
	private Date lastLogin;
	private Date generated;
	private String token;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Date getGenerated() {
		return generated;
	}

	public void setGenerated(Date generated) {
		this.generated = generated;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
