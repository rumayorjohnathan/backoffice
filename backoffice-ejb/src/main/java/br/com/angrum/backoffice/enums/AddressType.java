package br.com.angrum.backoffice.enums;

public enum AddressType {

	HOME(1, "Casa"), OFFICE(2, "Escritório"), OTHER(3, "Outro");

	private AddressType(Integer id, String description) {
		this.id = id;
		this.description = description;
	}

	private Integer id;
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
