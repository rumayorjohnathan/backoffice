package br.com.angrum.backoffice.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.angrum.backoffice.util.persistence.IPojo;

@Entity
@Table(schema = IPojo.BACKOFFICE_SCHEMA, name = "user", uniqueConstraints = @UniqueConstraint(columnNames = "login"))
@SequenceGenerator(schema = IPojo.BACKOFFICE_SCHEMA, name = "seq_user_id", sequenceName = "seq_user", allocationSize = 1, initialValue = 1)
public class User implements IPojo<Long> {

	private static final long serialVersionUID = 2113478039023174895L;

	private Long id;
	private String name;
	private String password;
	private String login;
	private Group group;
	private Date created;
	private Date lastLogin;
	private List<Phone> phones;
	private List<Email> emails;
	private List<Address> addresses;

	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "seq_user_id", strategy = GenerationType.SEQUENCE)
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	@NotNull(message = "Nome não pode estar vazio!")
	@NotEmpty(message = "Nome não pode estar vazio!")
    @Size(min = 1, max = 100, message = "Nome deve conter entre 1 e 100 caracteres!")
	@Pattern(regexp = "[^0-9]*", message = "Nome não deve conter números!")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "login")
	@NotNull(message = "Login não pode estar vazio!")
	@NotEmpty(message = "Login não pode estar vazio!")
    @Size(min = 8, max = 32, message = "Login deve conter entre 8 e 32 caracteres!")
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "password")
	@NotNull(message = "Senha não pode estar vazio!")
	@NotEmpty(message = "Senha não pode estar vazio!")
    @Size(min = 8, max = 16, message = "Senha deve conter entre 1 e 100 caracteres!")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToOne
	@NotNull(message = "Grupo não pode estar vazio!")
	@JoinColumn(name = "group_id", referencedColumnName = "id")
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created")
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_login")
	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

}
