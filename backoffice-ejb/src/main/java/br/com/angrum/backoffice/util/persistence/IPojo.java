package br.com.angrum.backoffice.util.persistence;

import java.io.Serializable;

public interface IPojo<T> extends Serializable {
	
	public static final String BACKOFFICE_SCHEMA = "backoffice";
	
	T getId();

}
