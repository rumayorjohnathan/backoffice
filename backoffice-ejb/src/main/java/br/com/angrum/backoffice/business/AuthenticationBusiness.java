package br.com.angrum.backoffice.business;

import java.math.BigInteger;
import java.security.MessageDigest;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.angrum.backoffice.repository.IUserRepository;
import br.com.angrum.backoffice.util.session.Access;
import br.com.angrum.backoffice.util.session.Credentials;

@Named
@Stateless
public class AuthenticationBusiness implements IAuthenticationBusiness {

	private static final long serialVersionUID = -3639026355589698640L;
	
	@Inject
	private IUserRepository userRepository;
	
	@Override
	public Credentials login(Access access) {
		return userRepository.login(access.getUsername(), getSHA512(access.getPassword()));
	}
	
	private String getSHA512(String input) {
		String toReturn = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			digest.reset();
			digest.update(input.getBytes("utf8"));
			toReturn = String.format("%040x", new BigInteger(1, digest.digest()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return toReturn;
	}

}
