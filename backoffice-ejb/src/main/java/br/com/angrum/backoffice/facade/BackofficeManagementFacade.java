package br.com.angrum.backoffice.facade;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.angrum.backoffice.business.IAuthenticationBusiness;
import br.com.angrum.backoffice.business.IGroupBusiness;
import br.com.angrum.backoffice.entity.Group;
import br.com.angrum.backoffice.filter.GroupFilter;
import br.com.angrum.backoffice.util.session.Access;
import br.com.angrum.backoffice.util.session.Credentials;

@Named
@Stateless
public class BackofficeManagementFacade implements IBackofficeManagementFacade {

	private static final long serialVersionUID = 8372738114603641874L;
	
	@Inject
	private IAuthenticationBusiness authenticationBusiness;
	
	@Inject
	private IGroupBusiness groupBusiness;
	
	@Override
	public Credentials login(Access access) {
		return authenticationBusiness.login(access);
	}

	@Override
	public List<Group> search(GroupFilter groupFilter) {
		return groupBusiness.search(groupFilter);
	}

	@Override
	public Group findById(Long id) {
		return groupBusiness.findById(id);
	}

	@Override
	public void save(Group group) {
		groupBusiness.save(group);
	}

	@Override
	public void delete(Long id) {
		groupBusiness.delete(id);
	}

}
