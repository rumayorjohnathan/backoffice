package br.com.angrum.backoffice.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import br.com.angrum.backoffice.entity.Group;
import br.com.angrum.backoffice.filter.GroupFilter;
import br.com.angrum.backoffice.util.session.Access;
import br.com.angrum.backoffice.util.session.Credentials;

@Local
public interface IBackofficeManagementFacade extends Serializable {
	
	Credentials login(Access access);
	
	List<Group> search(GroupFilter groupFilter);
	
	Group findById(Long id);
	
	void save(Group group);
	
	void delete(Long id);

}
