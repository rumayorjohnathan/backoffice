package br.com.angrum.backoffice.business;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import br.com.angrum.backoffice.entity.Group;
import br.com.angrum.backoffice.filter.GroupFilter;

@Local
public interface IGroupBusiness extends Serializable {
	
	List<Group> search(GroupFilter groupFilter);
	
	Group findById(Long id);
	
	void save(Group group);
	
	void delete(Long id);

}
