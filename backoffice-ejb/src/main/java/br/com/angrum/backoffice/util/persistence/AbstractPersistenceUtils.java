package br.com.angrum.backoffice.util.persistence;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractPersistenceUtils<T extends IPojo<?>> implements IPersistenceUtils<T> {

	private static final long serialVersionUID = -2172321013463692953L;
	private static Logger LOG = LoggerFactory.getLogger(IPersistenceUtils.class);

	@Override
	public CriteriaBuilder getCriteriaBuilder() {
		return getEntityManager().getCriteriaBuilder();
	}

	@Override
	public CriteriaQuery<T> createCriteriaQuery(Class<T> clazz) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
		return criteriaQuery;
	}

	@Override
	public Criteria createCriteria(Class<T> clazz) {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(clazz);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria;
	}

	@Override
	public Criteria createCriteria(Class<T> clazz, String alias) {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(clazz, alias);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria;
	}

	@Override
	public void logQuery(CriteriaQuery<T> criteriaQuery) {
		TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
		LOG.info(typedQuery.unwrap(Query.class).getQueryString());
	}

	@Override
	public Set<T> getResultSet(Criteria criteria) {
		Set<T> setResult = new HashSet<T>(getResultList(criteria));
		return setResult;
	}

	@Override
	public List<T> getResultList(Criteria criteria) {
		@SuppressWarnings("unchecked")
		List<T> listResult = criteria.list();
		return listResult;
	}

	@Override
	public List<T> getResultList(CriteriaQuery<T> criteriaQuery) {
		criteriaQuery.distinct(true);
		TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
		return typedQuery.getResultList();
	}

	@Override
	public Set<T> getResultSet(CriteriaQuery<T> criteriaQuery) {
		Set<T> setResult = new HashSet<T>(getResultList(criteriaQuery));
		return setResult;
	}

	@Override
	public T getSingleResult(CriteriaQuery<T> criteriaQuery) {
		criteriaQuery.distinct(true);
		TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
		// typedQuery.setMaxResults(1);
		List<T> results = typedQuery.getResultList();
		if (results != null && !results.isEmpty()) {
			return results.get(0);
		} else {
			return null;
		}
	}

	@Override
	public T getSingleResult(Criteria criteria) {
		// criteria.setMaxResults(1);
		List<T> listResult = getResultList(criteria);
		if (listResult != null && listResult.size() > 0) {
			return listResult.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void saveAndFlush(T pojo) {
		if (pojo.getId() != null) {
			getEntityManager().merge(pojo);
		} else {
			getEntityManager().persist(pojo);
		}
		getEntityManager().flush();
	}

	@Override
	public void saveAndDetach(T pojo) {
		saveAndFlush(pojo);
		getEntityManager().detach(pojo);
	}

	@Override
	public void save(T pojo) {
		if (pojo.getId() != null) {
			getEntityManager().merge(pojo);
		} else {
			getEntityManager().persist(pojo);
		}
	}

	@Override
	public void saveList(List<T> listPojo) {
		for (T pojo : listPojo) {
			save(pojo);
		}
	}

	@Override
	public void removeList(List<T> listPojo) {
		for (T pojo : listPojo) {
			// getEntityManager().detach(pojo);
			remove(pojo);
		}
	}

	@Override
	public void remove(T pojo) {
		getEntityManager().remove(pojo);
	}
	
	@Override
	public void removeAndFlush(T pojo) {
		getEntityManager().remove(pojo);
		getEntityManager().flush();
	}

	@Override
	public T getPorId(Class<T> clazz, Object id) {
		T pojo = getEntityManager().find(clazz, id);
		if (pojo == null) {
			return null;
		} else {
			return pojo;
		}
	}

	@Override
	public void flushAndClear() {
		getEntityManager().flush();
		getEntityManager().clear();
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> listarTodos(Class clazz) {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(clazz);
		return criteria.list();
	}

}
