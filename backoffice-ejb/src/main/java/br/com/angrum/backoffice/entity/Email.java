package br.com.angrum.backoffice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.angrum.backoffice.util.persistence.IPojo;

@Entity
@Table(schema = IPojo.BACKOFFICE_SCHEMA, name = "email")
@SequenceGenerator(schema = IPojo.BACKOFFICE_SCHEMA, name = "seq_email_id", sequenceName = "seq_email", allocationSize = 1, initialValue = 1)
public class Email implements IPojo<Long> {

	private static final long serialVersionUID = 411053134052704992L;

	private Long id;
	private String description;
	private String address;
	private User user;

	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "seq_email_id", strategy = GenerationType.SEQUENCE)
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@org.hibernate.validator.constraints.Email(message = "E-Mail não é válido!")
	@NotNull(message = "E-Mail não pode estar vazio!")
	@NotEmpty(message = "E-Mail não pode estar vazio!")
	@Size(min = 5, max = 50, message = "E-Mail deve conter entre 5 e 50 caracteres!")
	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
