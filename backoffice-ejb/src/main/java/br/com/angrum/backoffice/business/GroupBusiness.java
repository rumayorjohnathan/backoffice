package br.com.angrum.backoffice.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.angrum.backoffice.entity.Group;
import br.com.angrum.backoffice.filter.GroupFilter;
import br.com.angrum.backoffice.repository.IGroupRepository;

@Named
@Stateless
public class GroupBusiness implements IGroupBusiness {

	private static final long serialVersionUID = -1947059466555932429L;
	
	@Inject
	private IGroupRepository groupRepository;

	@Override
	public List<Group> search(GroupFilter groupFilter) {
		return groupRepository.search(groupFilter);
	}

	@Override
	public Group findById(Long id) {
		return groupRepository.findById(id);
	}

	@Override
	public void save(Group group) {
		groupRepository.save(group);
	}

	@Override
	public void delete(Long id) {
		groupRepository.delete(id);
		
	}

}
