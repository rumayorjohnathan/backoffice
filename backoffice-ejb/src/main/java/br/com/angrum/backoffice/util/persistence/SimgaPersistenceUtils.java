package br.com.angrum.backoffice.util.persistence;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import br.com.angrum.backoffice.util.annotation.BackofficePersistence;

@Named
@Stateless
@BackofficePersistence
public class SimgaPersistenceUtils<T extends IPojo<?>> extends AbstractPersistenceUtils<T> implements IPersistenceUtils<T> {

	private static final long serialVersionUID = -2051017819622314485L;

	@Inject
	@BackofficePersistence
	private EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

}
